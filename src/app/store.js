import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
import counterReducer from "../features/counter/counterSlice";
import alarmReducer from "../features/alarm/alarmSlice";

export const store = configureStore({
  reducer: {
    alarm: alarmReducer,
  },
  middleware: [logger]
});
