import React from "react";
import AppHeader from "../common/components/AppHeader/AppHeader";
import MainClock from "../common/components/MainClock/MainClock";

export default function App() {
  return (
    <>
      <AppHeader />
      <MainClock />
      <h3>
        알람 리스트
      </h3>
      <div>
        여기에는 알람 리스트가 들어갈 예정
      </div>
    </>
  );
}
