import { format } from "date-fns";

export default function formatDate(year, month, date, hour, minute, second) {
  return format(
    new Date(
      Number(year),
      Number(month),
      Number(date),
      Number(hour),
      Number(minute),
      Number(second),
    ),
    "yyyy-M-dd E aa H:mm:ss"
  );
}
