import React from "react";
import styled from "styled-components";
// import CloseButton from "../CloseButton/CloseButton";
import ModalPortal from "../Portal/Portal";

export default function Modal({ children, handleModalClose }) {
  return (
    <ModalPortal>
      <ModalBackground onClick={handleModalClose}>
        <div
          className="modalContainer"
          onClick={(e) => e.stopPropagation()}
        >
          {/* <CloseButton handleModalClose={handleModalClose}/> */}
          {children}
        </div>
      </ModalBackground>
    </ModalPortal>
  );
}

const ModalBackground = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: rgba(0,0,0,0.20);
  z-index: 0;

  .modalContainer {
    overflow: auto;
    overflow-x: hidden;
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 500px;
    height: 800px;
    max-height: 100%;
    padding: 25px;
    background: rgb(255, 255, 255);
    border-radius: 10px;
    text-align: center;
    box-shadow:
      0px 24px 38px 3px rgb(0 0 0 / 20%),
      0px 9px 46px 8px rgb(0 0 0 / 20%),
      0px 11px 15px -7px rgb(0 0 0 / 20%);
  }
`
