import React, { useState, useEffect } from "react";
import formatDate from "../../utils/formatDate";

export default function MainClock() {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
    setInterval(() => {
      setTime(new Date());
    }, 1000);

    return () => clearInterval;
  }, []);

  const formattedTime = formatDate(
    time.getFullYear(),
    time.getMonth(),
    time.getDate(),
    time.getHours(),
    time.getMinutes(),
    time.getSeconds()
  );

  return (
    <>
      <div>{formattedTime}</div>
    </>
  );
}