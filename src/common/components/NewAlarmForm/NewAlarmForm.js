import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { newAlarm } from "../../../features/alarm/alarmSlice"
import styled from "styled-components";
import Button from "../shared/Button";

export default function NewAlarmForm() {
  const [newAlarmData, setNewAlarmData] = useState({
    time: "",
    title: "",
    mode: ""
  });
  const dispatch = useDispatch();

  const handleSubmit = (ev) => {
    dispatch(newAlarm(newAlarmData));
    ev.preventDefault();
  };

  console.log(newAlarmData);

  return (
    <>
      <form onSubmit={handleSubmit}>
        <label>알람 시간: </label>
        <input type="datetime-local" onChange={(ev) => setNewAlarmData({...newAlarmData, time: ev.target.value})}/><br />
        <label>알람 이름: </label>
        <input type="text" placeholder="알람 이름" onChange={(ev) => setNewAlarmData({...newAlarmData, title: ev.target.value})} /><br />
        <label><input type="radio" name="mode" value="normal" onChange={(ev) => setNewAlarmData({...newAlarmData, mode: ev.target.value})} /> 일반</label>
        <label><input type="radio" name="mode" value="emergency" onChange={(ev) => setNewAlarmData({...newAlarmData, mode: ev.target.value})} /> 긴급</label><br />
        <Button type="submit">알람 등록</Button>
      </form>
    </>
  );
}