import React, { useState } from "react";
import Button from "../shared/Button";
import Modal from "../Modal/Modal";
import ModalPortal from "../Portal/Portal";
import NewAlarmForm from "../NewAlarmForm/NewAlarmForm";

export default function AppHeader() {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleModalToggle = () => {
    isModalOpen ? setIsModalOpen(false) : setIsModalOpen(true);
  };

  return (
    <>
      <Button>일반</Button>
      <Button>진동</Button>
      <Button>야간</Button>
      <Button onClick={handleModalToggle}>알람 추가</Button>
      {isModalOpen &&
        <ModalPortal>
          <Modal handleModalClose={handleModalToggle}>
            <NewAlarmForm />
          </Modal>
        </ModalPortal>
      }
    </>
  );
}
