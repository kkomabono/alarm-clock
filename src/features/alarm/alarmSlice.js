import { createSlice } from "@reduxjs/toolkit";

// const time = new Date();

const initialState = {
  alarm: []
};

const alarmSlice = createSlice({
  name: "alarm",
  initialState,
  reducers: {
    newAlarm: (state, action) => {
      state.alarm.push(action.payload);
    },
  },
});

export const { newAlarm } = alarmSlice.actions;

export default alarmSlice.reducer;
